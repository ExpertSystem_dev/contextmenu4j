package org.c4j.win;

import java.io.IOException;
import javax.swing.JOptionPane;
import org.c4j.util.OSUtils;
/*
 * <p>
 * Class for finding whether the user is an admin or not
 * using {@code java.lang.ProcessBuilder}
 * </p>
 * @version 1.0.0
 * #ThreadSafe#
 */
public class AdminUtilsWin {
	/*
	 * Boolean value showing whether the user is admin or not.
	 */
	public static boolean isAdmin = isAdmin();
//--------------------------------------------------------------------------------------------------------------
	/*
	 * <p>
	 * This method is used to check if the user is an admin or not.
	 * </p>
	 * @return true if the user is an admin, false otherwise.
	 */
	public static boolean isAdmin(){
		String OS_NAME = OSUtils.OS_NAME; // Retrieve the name of the OS.
		boolean hasAdminPrivileges = false;// Temporary variable representing user's admin privileges.
		if(OS_NAME.contains("Windows")){ 
			hasAdminPrivileges = isWindowsAdmin(); // Check if the user is on Windows machine.
			return hasAdminPrivileges;
		}else if(OS_NAME.contains("Mac")){
			return hasAdminPrivileges;
		}else if(OS_NAME.contains("Ubuntu")){
			return hasAdminPrivileges;
		}else{
			return hasAdminPrivileges;
		}
	}
//--------------------------------------------------------------------------------------------------------------
	/*
	 * <p>
	 * This method determines if the user is an admin on a Windows machine.
	 * It used the Command Prompt command NET FILE to determine if user has
	 * admin privileges.
	 * </p>
	 */
	public static boolean isWindowsAdmin(){
		try {
			ProcessBuilder builder = new ProcessBuilder("NET FILE"); //Create a ProcessBuilder to execute the command
			Process process = builder.start(); //Start the process
			int exitValue = process.waitFor(); //Wait for the process to finish executing
			if(exitValue == 0){ //Check for normal or abnormal termination
				return true;
			}else{
				return false;
			}
		} catch (InterruptedException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
	}
}

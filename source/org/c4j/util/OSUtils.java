/*
 * OSUtils
 * 
 * Version 1.0.0
 * 
 * [insert copyright information here]
 * @author Little Java Child
 */
package org.c4j.util;


/*
 * <p>
 * Class for finding operating system information using {@code java.lang.System}
 * </p>
 * @version 1.0.0
 * #ThreadUnsafe#
 */
public class OSUtils {
	/*
	 * All Windows OS have a prefix 'Windows' eg. Windows XP, Windows 7, etc.
	 * This will be used to find if the user has a Windows OS.
	 */
	private static final String WINDOWS_OS_PREFIX = "Windows";
	
	/*
	 * All Mac OS X have a prefix 'Mac OS X' eg. Mac OS X Lion, Mac OS X Snow Leopard, etc.
	 */
	private static final String MAC_OS_X = "Mac OS X";
	
	/*
	 * All Ubuntu OS have a prefix 'Ubuntu'.
	 */
	//TODO check if this is true. Not sure.
	private static final String UBUNTU = "Ubuntu";
	
	/*
	 * System property constants that will be used to retrieve information
	 * about OS, current directory, user home directory, etc.
	 * All other variables depend on these.
	 */
	private static final String USER_DIR_PROP = "user.dir"; // Current directory.
	private static final String USER_HOME_PROP = "user.home"; // Home directory.
	private static final String JAVA_HOME_PROP = "java.home"; // Java home directory.
	private static final String OS_NAME_PROP = "os.name"; // Operating System name.
	private static final String OS_ARCH_PROP = "os.arch"; // Architecture of OS.
	private static final String OS_VER_PROP = "os.version"; // Operating System version.
	private static final String FILE_SEPARATOR_PROP = "file.separator"; //Default file separator
	
	/*
	 * Variables containing the values for different system properties.
	 */
	public static final String USER_DIR = getSystemProperty(USER_DIR_PROP);
	public static final String USER_HOME = getSystemProperty(USER_HOME_PROP);
	public static final String JAVA_HOME = getSystemProperty(JAVA_HOME_PROP);
	public static final String OS_NAME = getSystemProperty(OS_NAME_PROP);
	public static final String OS_ARCH = getSystemProperty(OS_ARCH_PROP);
	public static final String OS_VER = getSystemProperty(OS_VER_PROP);
	public static final String FILE_SEPARATOR = getSystemProperty(FILE_SEPARATOR_PROP);
	
	/*
	 * Boolean values to quickly check what OS the user is on. These values depend upon
	 * the constants declared above.
	 * Determining Windows OS is slightly different from determining other OS.
	 */
	public static final boolean IS_WINDOWS_XP = isOS(WINDOWS_OS_PREFIX,"5.1");
	public static final boolean IS_WINDOWS_7 = isOS(WINDOWS_OS_PREFIX,"6.1");
	public static final boolean IS_WINDOWS_8 = isOS(WINDOWS_OS_PREFIX,"6.2");
	public static final boolean IS_MAC = isOS(MAC_OS_X);
	public static final boolean IS_UBUNTU = isOS(UBUNTU);
	
//--------------------------------------------------------------------------------------------------------------
	/*
	 * <p>
	 * Retrieves a system property.
	 * If a {@code SecurityException} is caught, it will return null and print an error
	 * message using {@code System.err}
	 * </p>
	 * @param system property to be retrieved
	 * @return the property or null if an exception is caught.
	 */
	public static String getSystemProperty(final String property){
		try{
			return System.getProperty(property);
		}catch(final SecurityException e){
			System.err.println("Permission denied to read system property: " + 
							property + ". Using default value of null");
			return null;
		}
	}
//--------------------------------------------------------------------------------------------------------------
	/*
	 * <p>
	 * This is used to determine precisely what version of Windows the user is currently
	 * on.
	 * </p>
	 * @param name of the OS, version of the OS
	 * @return true if matches, false otherwise.
	 */
	public static boolean isOS(final String name,final String version){
		if(name == null || version == null){
			return false;
		}else{
			if(OS_NAME.contains(name) && OS_VER.contains(version)){
				return true;
			}else{
				return false;
			}
		}
	}
//--------------------------------------------------------------------------------------------------------------
	/*
	 * <p>
	 * This is used to determine what version of the OS the user is on.
	 * This method is used in case of non-Windows OS.
	 * </p>
	 * @param name of the OS
	 * @return true if matches, false otherwise.
	 */
	public static boolean isOS(final String name){
		if(name == null){
			return false;
		}else{
			if(OS_NAME.contains(name)){
				return true;
			}else{
				return false;
			}
		}
	}
//--------------------------------------------------------------------------------------------------------------
}
